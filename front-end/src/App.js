import logo from './flat-earth.png';
import './App.css';
import React, { Component } from 'react';


class App extends Component {

  state = {
    count: 0
  }

  componentDidMount() {
    var { count } = this.state;
    count = localStorage.getItem('count') || 0;
    count++;
    localStorage.setItem('count', count);
    this.setState({ count });
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Latest weatherforcast coming soon™
        </p>
        </header>
      </div>
    )
  }
}

export default App;
